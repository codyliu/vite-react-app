import React, { Component } from 'react'
import { Layout, Menu, Breadcrumb, Spin } from 'antd'
import IconFont from '@component/IconFont'
import Nav from '@layout/Nav'
import { Switch, Route, withRouter } from 'react-router-dom'
import routes from '../route'
const { Header, Content, Footer, Sider } = Layout
const { SubMenu } = Menu
import { SmileTwoTone } from '@ant-design/icons'
import { connect } from 'react-redux'

interface Myroute {
  path: string // 必要属性
  name: string
  component: any
  icon: string
  children: Array<Myroute>
}
class SiderLayout extends Component<any, any> {
  constructor(props: any) {
    super(props)
    this.state = {
      collapsed: false,
      tabNav: ['tab1', 'tab2']
    }
    this.goRouter = this.goRouter.bind(this)
  }
  goRouter(item: any) {
    console.log(item.key, this)
    this.props.history.push(item.key)
  }
  onCollapse = (collapsed: any) => {
    this.setState({ collapsed })
  }
  eachRoute(route: any) {
    return route.map((e: Myroute) => {
      if (e.children) {
        return e.children.map((ee: any) => {
          return <Route key={ee.path} path={ee.path} component={ee.component} />
        })
      }
      return <Route key={e.path} path={e.path} component={e.component} />
    })
  }
  render() {
    const { collapsed } = this.state
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Sider collapsible collapsed={collapsed} onCollapse={this.onCollapse}>
          <div className="logo" />
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" onClick={this.goRouter}>
            {routes.map((route) => {
              return !route.children?.length ? (
                <Menu.Item key={route.path}>
                  <div>
                    <IconFont type={route.icon} /> <span>{route.name}</span>
                  </div>
                </Menu.Item>
              ) : (
                <SubMenu key={`${route.path}sub1`} icon={<SmileTwoTone />} title={route.name}>
                  {route.children.map((childRoute) => {
                    return (
                      <Menu.Item key={childRoute.path}>
                        <div>
                          <IconFont type={childRoute.icon} />
                          {childRoute.name}
                        </div>
                      </Menu.Item>
                    )
                  })}
                </SubMenu>
              )
            })}
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            <Nav />
          </Header>
          <Content style={{ margin: '0 16px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
              {this.state.tabNav.map((nav: any) => {
                return <Breadcrumb.Item key={nav}>{nav.name}</Breadcrumb.Item>
              })}
            </Breadcrumb>
            <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
              <Spin spinning={this.props.loading}>
                <Switch>{this.eachRoute(routes)}</Switch>
              </Spin>
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
        </Layout>
      </Layout>
    )
  }
}
const mapStateToProps = (state: any) => state
export default connect(mapStateToProps)(withRouter(SiderLayout))
