import { PageHeader } from 'antd'
import React from 'react'
import AvatarCom from '@layout/AvatarCom'
import './Nav.module.css'
const Nav = () => {
  return (
    <div className="site-page-header-ghost-wrapper">
      <PageHeader
        ghost={false}
        onBack={() => window.history.back()}
        title="Title"
        subTitle="This is a subtitle"
        extra={[
          <AvatarCom
            key="1"
            count={1}
            src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
          />
        ]}
       />
    </div>
  )
}

export default Nav
