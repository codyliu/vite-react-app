import { handleActions } from 'redux-actions'
import { createStore } from 'redux'

const defaultState = {
  loading: false
}
const reducers = handleActions({
  //使loading显示
  GALOB_DATA: (state, payload) => ({ ...state, ...payload.payload }),
  //使loading隐藏
  GLOAD_FLASE: (state, payload) => ({ ...state, ...payload.payload })
}, defaultState)
const store = createStore(reducers)
export default store

