import React from 'react';
import {Spin} from 'antd';
import {LoadingOutlined} from '@ant-design/icons';
import './Loading.module.css';
const antIcon = <LoadingOutlined style={{fontSize: 24}} spin />;
const Loading = () => {
  return (
    <div className="box">
      <div className="loading">
        <Spin indicator={antIcon} />
      </div>
    </div>
  );
};
export default Loading;
