import React from 'react'
import './SearchHeader.moudle.css'
import { PageHeader, Input, Col, Row, Select, Button } from 'antd'
const { Option } = Select
const options = [
  {
    value: 'zhejiang',
    label: 'Zhejiang',
    children: [
      {
        value: 'hangzhou',
        label: 'Hangzhou',
        children: [
          {
            value: 'xihu',
            label: 'West Lake'
          }
        ]
      }
    ]
  },
  {
    value: 'jiangsu',
    label: 'Jiangsu',
    children: [
      {
        value: 'nanjing',
        label: 'Nanjing',
        children: [
          {
            value: 'zhonghuamen',
            label: 'Zhong Hua Men'
          }
        ]
      }
    ]
  }
]
const SearchHeader = () => {
  return (
    <PageHeader ghost={false}>
      <div className="site-input-group-wrapper">
        <Input.Group size="large">
          <Row gutter={8}>
            <Col span={5}>
              <Input defaultValue="0571" />
            </Col>
            <Col span={5}>
              <Input defaultValue="26888888" />
            </Col>
            <Col span={3}>
              <Select defaultValue="Zhejiang">
                <Option value="Zhejiang">Zhejiang</Option>
                <Option value="Jiangsu">Jiangsu</Option>
              </Select>
            </Col>
            <Col span={5}>
              <Button type="primary" size="large">
                搜索
              </Button>
            </Col>
          </Row>
        </Input.Group>
      </div>
    </PageHeader>
  )
}

export default SearchHeader
