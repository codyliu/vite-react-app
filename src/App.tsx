import React, { Component } from 'react'
import './App.css'
import SiderLayout from '@layout/SiderLayout'
import { BrowserRouter as Router } from 'react-router-dom'
class App extends Component<any, any> {
  constructor(props: any) {
    super(props)
    this.state = {};
  }
  render() {
    return (
      <Router>
        <SiderLayout />
      </Router>
    )
  }
}

export default App
