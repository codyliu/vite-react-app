import Mock from 'mockjs'


Mock.mock('/api/data', 'get', {
  code: 0,
  "data|9-19": [
    { label: /[a-z]{3,5}/, "value|+1": 99, },
  ]
})
Mock.mock('/api/user', 'get', {
  code: 0,
  data: {
    name: 'nick',
    age: 20,
    isuper: true
  }
})
Mock.mock('/api/isSuper', 'get', {
  code: 0,
  "data|9-19": [
    { label: /[a-z]{3,5}/, "value|+1": 99, },
  ]
})